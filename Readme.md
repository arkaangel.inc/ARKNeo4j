Neo4jBundle
================
##Description
This is the bundle to use Neo4j.

##Default configuration
By default it is used as protocol 'bolt' and port '7687' and hostname 'neo4j'.

##Minimal configuration
The necessary minimal configuration is:

```php
<?php
$conf['neo4j']=[
    "user"=>"user",
    "pass"=>"pass"
];
```

##In addition you can define the protocol, port and hostname in the following way:

```php
<?php
$conf['neo4j']=[
    "protocol"=>"bolt"|"http",
    "host"=>"hostname",
    "port"=>123456
];
```