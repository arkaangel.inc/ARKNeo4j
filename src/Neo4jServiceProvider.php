<?php
/**
 * Created by PhpStorm.
 * User: jaime
 * Date: 14/06/18
 * Time: 12:29
 */

namespace Arkaangel\Neo4jBundle;

use GraphAware\Neo4j\OGM\EntityManager;
use Pimple\ServiceProviderInterface;
use Pimple\Container;
use Symfony\Component\Translation\Exception\NotFoundResourceException;


class Neo4jServiceProvider implements ServiceProviderInterface
{
    const DEFAULT_PROTOCOL = 'bolt';
    const DEFAULT_HOST     = 'neo4j';
    const DEFAULT_PORT     = '7687';

    public function register(Container $app)
    {
        $app['ogm.em'] = function () use ($app) {
            $protocol = isset($app['neo4j']['protocol']) ? $app['neo4j']['protocol'] : Neo4jServiceProvider::DEFAULT_PROTOCOL;
            $host = isset($app['neo4j']['host']) ? $app['neo4j']['host'] : Neo4jServiceProvider::DEFAULT_HOST;
            $port = isset($app['neo4j']['port']) ? $app['neo4j']['port'] : Neo4jServiceProvider::DEFAULT_PORT;

            if (!isset($app['neo4j']['user']))
                throw new NotFoundResourceException("User for Neo4j not found.");
            $user = $app['neo4j']['user'];

            if (!isset($app['neo4j']['pass']))
                throw new NotFoundResourceException("Password for Neo4j not found.");
            $pass = $app['neo4j']['pass'];

            return EntityManager::create(sprintf('%s://%s:%s@%s:%d', $protocol, $user, $pass, $host, $port));
        };

    }
}